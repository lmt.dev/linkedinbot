package com.stcor.ScraperBot.Services;
import com.stcor.ScraperBot.Bots.model.BasicBot;
import com.stcor.ScraperBot.Bots.model.linkedin.Contact;
import com.stcor.ScraperBot.Bots.model.linkedin.Filter;
import com.stcor.ScraperBot.repository.linkedin.ContactRepository;
import com.stcor.ScraperBot.repository.linkedin.FilterRepository;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;

public class Linkedin_Recruiting_Service implements BotService {
    
    @Autowired
    private ContactRepository contactRepository;
    
    @Autowired
    public void setContactRepository(ContactRepository contactRepository) {
        
        this.contactRepository = contactRepository;
    }
    
    @Autowired
    private FilterRepository filterRepository;
    
    @Autowired
    public void setFilterRepository(FilterRepository contactRepository) {
        
        this.filterRepository = filterRepository;
    }
    
    @Override
    public void start(){
        try {
            BasicBot bot = new BasicBot();
            bot.InicializarWebdriver();
            
            //ContactCleaning(bot);
            
            filterList = filterRepository.findAll();
            
            
            while(true){
                int indexCard = 1;
                indexStalker = 0;
                isOK = true;
                bot.IrA("https://www.linkedin.com/mynetwork/");
                while(isOK && indexCard<3000){

                    Scrolling(bot);
                    Thread.sleep(10000);
                    inProcess = true;
                    while(inProcess){
                        try{
                             Contact newContact = new Contact();
                             //sometimes the xpath is moved
                             if (indexStalker==0){
                                 int indexStalkerFinder=7;
                                 while(newContact.getUrl()==null){
                                     newContact = Stalker(bot, newContact, indexCard, indexStalkerFinder);
                                     if(newContact.getUrl()!=null){
                                         indexStalker=indexStalkerFinder;
                                         System.out.println("New indexStalker: "+indexStalker);
                                     }
                                     else{
                                         indexStalkerFinder++;
                                     }                                
                                 }
                             }
                             else{
                                 newContact = Stalker(bot, newContact, indexCard, indexStalker);
                             }         


                             if (newContact.getName()!=null){
                                 System.out.println("Id: "+ indexCard + " - Name: "+newContact.getName()+" - Job: "+newContact.getJob());

                                 Contact findNewContact = contactRepository.findByUrl(newContact.getUrl());
                                 if (findNewContact==null){
                                     //if it's everything OK, creation and connection
                                     newContact.setCreated(new Date());
                                     Connection(bot, newContact, indexCard, indexStalker);
                                 }
                                 else{
                                     //when is found, visit
                                      Date date = new Date();
                                      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                      Calendar c = Calendar.getInstance();
                                      c.setTime(date);
                                      c.add(Calendar.HOUR, -24);
                                      System.out.println(c.getTime());
                                     if (findNewContact.getConnected().after(c.getTime())){
                                         System.out.println("Contact invited 24 hours ago: "+findNewContact.getUrl());
                                     }
                                 }

                             }
                             else{
                                 inProcess=false;
                             }

                             //System.out.println("Reading more");

                         }
                         catch(Exception ex){
                             System.out.println("starting...");
                         }
                        indexCard++;
                    }
                }   
            }
            
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
    }
    private int indexStalker;
    
    private boolean isOK;
    
    private boolean inProcess;
    
    protected void Scrolling(BasicBot bot){
        try{
            int dropdown = 10;
            while(dropdown>0){
                Thread.sleep(1000);
                bot.EjecutarScript("window.scrollBy(0,900)");
                //System.out.println("Scrolling...");
                dropdown--;
            }
        }
        catch(Exception ex){
            
        }
    }
    
    private List<Filter> filterList;
    private WebElement we = null;
    
    protected Contact Stalker(BasicBot bot, Contact newContact, int indexCard, int indexStalk){
        try{
        we = bot.Xpath(
                "/html/body/div["+indexStalk+"]/div[3]/div/div/div/div/div/div/div[2]/section/section/ul/li["+indexCard+"]/div/section/div[1]/a/span[2]");
        newContact.setName(we.getText());
        }
        catch(Exception ex){

        }

        try{
        we = bot.Xpath(
                "/html/body/div["+indexStalk+"]/div[3]/div/div/div/div/div/div/div[2]/section/section/ul/li["+indexCard+"]/div/section/div[1]/a/span[4]");
        newContact.setJob(we.getText());
        
        }
        catch(Exception ex){

        }

        try{
        we = bot.Xpath(
                "/html/body/div["+indexStalk+"]/div[3]/div/div/div/div/div/div/div[2]/section/section/ul/li["+indexCard+"]/div/section/div[1]/a");
        newContact.setUrl(we.getAttribute("href"));
        }
        catch(Exception ex){

        }

        try{
        we = bot.Xpath(
                "//html/body/div["+indexStalk+"]/div[3]/div/div/div/div/div/div/div[2]/section/section/ul/li["+indexCard+"]/div/section/div[1]/a/img");
        newContact.setImg(we.getAttribute("src"));
        }
        catch(Exception ex){

        }
        return newContact;
    }
    
    protected void ContactCleaning(BasicBot bot){
        try{
            // first. remove pending invitations
            int indexPage = 2;
            while(indexPage > 1){                
                bot.IrA("https://www.linkedin.com/mynetwork/invitation-manager/sent/?invitationType=CONNECTION&page="+indexPage);
                Thread.sleep(1000);
                try{
                    int indexRow = 0;
                    while(indexRow<100){
                        
                        try{
                            indexRow++;
                            we = bot.Xpath(
                                    "/html/body/div[7]/div[5]/div/div/div/div/div/div/section/div[2]/div[2]/ul/li[1]/div/div[2]/button/span"
                            );
                            we.click();
                            Thread.sleep(1000);
                            we = bot.Xpath("/html/body/div[4]/div/div/div[3]/button[2]/span");
                            we.click();
                            Thread.sleep(1000);
                        }
                        catch(Exception ex){
                            
                        }
                        
                        try{
                            indexRow++;
                            we = bot.Xpath(
                                    "/html/body/div[8]/div[3]/div/div/div/div/div/div/section/div[2]/div[2]/ul/li[1]/div/div[2]/button/span"
                            );
                            we.click();
                            Thread.sleep(1000);
                            we = bot.Xpath("/html/body/div[4]/div/div/div[3]/button[2]/span");
                            we.click();
                            Thread.sleep(1000);
                        }
                        catch(Exception ex){
                            
                        }
                    }
                }
                catch(Exception ex)
                {
                    
                }
                indexPage--;
            }
        }
        catch(Exception ex){
            
        }
    }
            
    protected void Connection(BasicBot bot, Contact newContact, int indexCard, int indexStalk){
        try{
            String job = newContact.getJob().toLowerCase();
            
            job = job
                    .replace(".", " ")
                    .replace(",", " ")
                    .replace(";", " ")
                    .replace(":", " ")
                    .replace("-", " ")
                    ;
            
            String[] jobsArr = job.split(" ");
            
            int coincidences = 0;
            for(String word:jobsArr){
                for(Filter myFilt:filterList){
                    if(word.equals(myFilt.getName())){
                       //System.out.println("Filter '"+myFilt.getName()+"' equals "+word);
                       coincidences++;
                    }
                }
            }
            boolean black=false;
            String[] blackList = {
                "Contratamos talento para democratizar el comercio", 
                "Wings Mobile", 
                "ayi y asociados", 
                "ayi & asociados",
                "desarrollador",
                "developer",
                "development",
                "desarrollo",
                "backend",
                "frontend",
                "engineer",
                "ingeniero",
                "ingeniera",
                "programador",
                "programación"
            };
            
            if(coincidences>1){
                //Blacklist control
                for(String blacklisted:blackList){
                    if(newContact.getJob().toLowerCase().contains(blacklisted.toLowerCase())){
                        black=true;
                        System.out.println("BLACKLISTED: Id: "+ indexCard + " - Name: "+newContact.getName()+" - Job: "+newContact.getJob());
                        break;
                    }
                }
                
                if (!black){
                    System.out.println("Id: "+ indexCard + " - Name: "+newContact.getName()+" - Job: "+newContact.getJob());
                    we = bot.Xpath(
                    "/html/body/div["+indexStalk+"]/div[3]/div/div/div/div/div/div/div[2]/section/section/ul/li["+indexCard+"]/div/section/div[2]/footer/button");
                    we.click();
                    
                    newContact.setConnected(new Date());

                    contactRepository.save(newContact);
                    
                    System.out.println("Connection successfully!");
                    
                    Thread.sleep(30000);
                    
                    try{
                        we = bot.Xpath(
                        "/html/body/div[4]/div/div/div[3]/button/span");
                        we.click();
                        //is enough
                        inProcess=false;
                        isOK=false;
                    }
                    catch(Exception ex){
                        //its OK
                        we = bot.Xpath(
                        "/html/body/div[4]/div/div/button/li-icon/svg");
                        we.click();
                    }
                }
            }

        }
        catch(Exception ex){
            
        }
    }
    
}
